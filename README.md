# Desafio - ioasys

Projeto criado em React para realizar um desafio proposto pela _ioasys_ para a vaga de pessoa desenvolvedora front-end.

### Aplicação em produção
- https://caiookb.github.io/empresas/#/

### Principais bibliotecas utilizadas

- "**axios"**: Para uma melhor requisição http;
- **"react-redux"/"redux"/"redux-thunk"** : Bibliotecas para o funcionamento do Redux na aplicação;
- **"react-router-dom"**: Utilização de rotas;
- **"@testing-library/react"**: Para executar testes na aplicação;.

### Como executar a aplicação

- Pré-requisitos: NodeJs

1 - Clone o repositório e execute o comando `npm install` para instalar todas as bibliotecas necessárias para funcionamento do projeto.

2 - Execute o comnado `npm start`

3 - Navegue para `localhost:3000`

